package org.academiadecodigo.argicultores;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Field {
    private Rectangle gameArea;
    private Picture map;

    public Field(String img) {
        map = new Picture(10,10,img);
        map.draw();
        gameArea = new Rectangle(10, 10, 800, 850);
        gameArea.draw();
    }

    public int getRightLimit() {
        return gameArea.getWidth();
    }

    public int getBottoLimit() {
        return gameArea.getWidth();
    }

}
