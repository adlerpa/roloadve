package org.academiadecodigo.argicultores.maps;

import org.academiadecodigo.argicultores.maps.obstacles.Box;
import org.academiadecodigo.argicultores.maps.obstacles.Obstacle;
import org.academiadecodigo.argicultores.maps.obstacles.Obstacles;
import org.academiadecodigo.argicultores.maps.obstacles.Rock;

public class Map {
    Obstacles[] one;

    public Obstacles[] levelOne(){
        one = new Obstacles[]{
                new Box(130, 90),
                new Box(50, 130),
                new Box(50, 170),
                new Box(50, 210),
                new Box(50, 290),
                new Box(50,330),
                new Box(50, 370),
                new Box(130, 250),
                new Box(130, 410),
                new Box(130, 570),
                new Box(130, 730),
                new Box(530, 50),



        };
        return one;
    }
    public Obstacles[] getObstacles(){
        return one;
    }
}
